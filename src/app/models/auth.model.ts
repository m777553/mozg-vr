import {Validators} from "@angular/forms";

export interface UserAuth {
  email: string;
  password: string;
}

export interface UserDTO {
  token: string;
  user: User
}

export interface User {
  id: string;
  name: string;
  role: 'admin' | 'user' | 'localadmin';
  phone?: number | string;
  organization?: string;
  goal?: string;
  tariff?: string
  organisationId?: string
}

export interface SignUpUserDTO {
  name: string;
  surname: string;
  email: string;
  password: string;
  phone: number;
  organization: string;
  goal: string;
  tariff: string; // enum
  isLegalEntity: boolean;
  organizationName?: string,
  inn?: string,
  requisites?: string,
}
