export interface SessionDTO {
  id: number;
  userId?: number;
  userName: string;
  courseId?: number;
  courseName: string;
  totalTasksCount: number;
  finishedTasksCount: number;
}
