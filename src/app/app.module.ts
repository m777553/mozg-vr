import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LoginModule} from './components/login/login.module';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AdminHomeModule} from './components/admin-home/admin-home.module';
import {SignUpModule} from './components/sign-up/sign-up.module';
import {BaseUrlInterceptor} from './interceptors/base-url.interceptor';
import {UserHomeModule} from './components/user-home/user-home.module';
import {FooterModule} from "./components/shared/footer/footer.module";
import {AddMenteeDialogModule} from "./dialogs/add-mentee-dialog/add-mentee-dialog.module";
import {ArchiveMenteesModule} from "./modules/archive-mentees/archive-mentees.module";
import {CoursesModule} from "./modules/courses/courses.module";
import {HeaderModule} from "./components/shared/header/header.module";
import {MenteePageComponent} from "./modules/mentee-page/mentee-page.component";
import {MenteePageModule} from "./modules/mentee-page/mentee-page.module";

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LoginModule,
    HttpClientModule,
    AdminHomeModule,
    UserHomeModule,
    SignUpModule,
    FooterModule,
    AddMenteeDialogModule,
    ArchiveMenteesModule,
    CoursesModule,
    HeaderModule,
    MenteePageModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: BaseUrlInterceptor, multi: true},],
  bootstrap: [AppComponent],
})
export class AppModule {
}
