import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class BaseUrlInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const BASE_URL = 'https://stage-api.vr-mozg.ru/api/v1';
    const token = localStorage.getItem('token');
    if (token) {
      req = req.clone({
        setHeaders: {
          Authorization: `Bearer ${token}`,
        },
      });
    }
    if (req.url.includes('assets')) {
      return next.handle(req);
    }

    return next.handle(req.clone({ url: `${BASE_URL}/${req.url}` }));
  }
}
