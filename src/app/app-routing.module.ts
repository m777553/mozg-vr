import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AdminHomeComponent} from './components/admin-home/admin-home.component';
import {LoginComponent} from './components/login/login.component';
import {SignUpComponent} from './components/sign-up/sign-up.component';
import {AuthGuard} from './guards/auth.guard';
import {UserHomeComponent} from './components/user-home/user-home.component';
import {ArchiveMenteesComponent} from "./modules/archive-mentees/archive-mentees.component";
import {CoursesComponent} from "./modules/courses/courses.component";
import {MenteePageComponent} from "./modules/mentee-page/mentee-page.component";
import {RoleGuardService} from "./guards/role.guard";

const routes: Routes = [
  {
    path: 'admin-home',
    component: AdminHomeComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'user-home',
    component: UserHomeComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'archive-mentees',
    component: ArchiveMenteesComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'courses/:id',
    component: CoursesComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'courses',
    component: CoursesComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'sign-up',
    component: SignUpComponent,
  },
  {
    path: 'log-in',
    component: LoginComponent,
  },
  {path: 'user/:userId', component: MenteePageComponent},
  {
    path: '', // Маршрут по умолчанию
    canActivate: [RoleGuardService],
    children: [
      {
        path: '',
        redirectTo: 'user-home',
        pathMatch: 'full'
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
