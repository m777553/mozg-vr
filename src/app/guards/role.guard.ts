import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import {UserService} from "../services/user/user.service";

@Injectable({
  providedIn: 'root'
})
export class RoleGuardService implements CanActivate {

  constructor(public user: UserService, public router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    console.log('guard')
    if (this.user.currentUser?.role === 'admin') {
      this.router.navigateByUrl('/admin-home');
      return false;
    } else if (this.user.currentUser?.role === 'user' || this.user.currentUser?.role === 'localadmin') {
      this.router.navigateByUrl('/user-home');
      return false;
    }
    this.router.navigateByUrl('/log-in');
    return true;
  }
}
