export interface Balance {
  id: string | null;
  comment: string | null;
  lastPaymentDate: string;
  nextPaymentDate: string;
  tariffExpiration: string;
  value: number | null;
  currency: string | null;
}

export interface Pupil {
  id: string;
  isActive: boolean;
  name: string | null;
  comment: string | null;
  city: string | null;
  activePupils: string[];
  activeUsers: string[];
  archivedPupils: string[];
  sessions: any[];
  tariff: any | null;
  payments: any[];
  balance: Balance;
  inn: string | null;
  requisites: string | null;
}

export interface Organization {
  id: string;
  isActive: boolean;
  name: string | null;
  comment: string | null;
  city: string | null;
  activePupils: string[];
  activeUsers: string[];
  archivedPupils: string[];
  sessions: any[];
  tariff: any | null;
  payments: any[];
  balance: Balance;
  inn: string | null;
  requisites: string | null;
}
