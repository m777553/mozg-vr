import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {BehaviorSubject, of, switchMapTo, take} from 'rxjs';
import {filter, switchMap, tap} from 'rxjs/operators';
import {Mentee} from '../../services/mentee/mentee.model';
import {MenteeService} from '../../services/mentee/mentee.service';
import {CourseMenteeDetail, COURSES} from "./mock";


@Component({
  selector: 'mozg-mentee-page',
  templateUrl: './mentee-page.component.html',
  styleUrls: ['./mentee-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenteePageComponent implements OnInit {
  userId: string = '';
  menteeForm: FormGroup;
  mentee$ = new BehaviorSubject<Mentee | null>(null);
  courses: CourseMenteeDetail[] = [];
  displayedColumns: string[] = ['date', 'course', 'lesson', 'comment', 'result'];
  ages = Array.from({length: 16}, (_, i) => i + 3);
  initialFormValues: any;

  constructor(private _route: ActivatedRoute, private _menteeService: MenteeService, private fb: FormBuilder, private _router: Router) {
    this.menteeForm = this.fb.group({
      name: ['', Validators.required],
      surname: ['', Validators.required],
      age: ['', [Validators.required, Validators.min(3), Validators.max(18)]],
      gender: ['', Validators.required],
      interests: ['', Validators.required],
    });
  }

  ngOnInit() {
    this.userId = this._route.snapshot.paramMap.get('userId') ?? '';
    this._route.paramMap.pipe(
      tap(data => {
        console.log('params', data);
      }),
      filter(data => !!data),
      switchMap(params => {
        const userId = params.get('userId');
        return userId ? this._menteeService.getMenteeById(userId) : of(null);
      }),
      tap(data => {
        console.log('user', data);
        if (data) {
          this.menteeForm.patchValue(data);
          // this.courses = data.courses; // предполагается, что курсы являются частью данных подопечного
          this.courses = COURSES; // МОКИ
          this.initialFormValues = this.menteeForm.value;
        }
      }),
    ).subscribe();
  }

  addCourse(): void {
    this._router.navigate(['courses', this.userId]);
  }

  archiveMentee(): void {
    this._menteeService.moveToArchive(this.userId).pipe(
      take(1)
    ).subscribe(() => {
      this._router.navigate(['/']); // переадресация на главную страницу
    });
  }

  editMentee() {
    this._menteeService.editMenteeById(this.userId, this.menteeForm.value).pipe(take(1), tap(() => {
      this.initialFormValues = this.menteeForm.value;
    })).subscribe()
  }

  clearChanges() {
    this.menteeForm.reset(this.initialFormValues);
  }
}
