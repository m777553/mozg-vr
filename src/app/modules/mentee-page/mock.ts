
export interface CourseMenteeDetail {
  id: string;
  date: string;
  course: string;
  lesson: string;
  comment: string;
  result: string;
}

export const COURSES = [
  {
    id: 'hsgfjhs',
    date: '2022-01-01',
    course: 'Курс 1',
    lesson: 'Урок 1',
    comment: 'Хорошо справился',
    result: '100%'
  },
  {
    id: 'hsgdhfjhs',
    date: '2022-01-02',
    course: 'Курс 1',
    lesson: 'Урок 2',
    comment: 'Нужно повторить',
    result: '0%'
  },
  {
    id: 'gjk;',
    date: '2022-01-03',
    course: 'Курс 2',
    lesson: 'Урок 1',
    comment: 'Отлично справляется',
    result: '75%'
  },
];
