import {AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {Mentee} from '../../services/mentee/mentee.model';
import {MenteeService} from '../../services/mentee/mentee.service';
import {MatSort} from "@angular/material/sort";
import {Observable, switchMapTo, tap} from "rxjs";
import {MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'mozg-archive-mentees',
  templateUrl: './archive-mentees.component.html',
  styleUrls: ['./archive-mentees.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ArchiveMenteesComponent implements OnInit, AfterViewInit {
  dataSource= new MatTableDataSource<Mentee>([]);
  displayedColumns: string[] = ['surname', 'name', 'age', 'actions'];

  @ViewChild(MatSort) sort: MatSort | null = null;

  constructor(private _menteeService: MenteeService, private _cdr: ChangeDetectorRef) {}

  ngOnInit() {
    this.getMentees().subscribe();
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    console.log(this.dataSource.data)

  }

  getMentees(): Observable<Mentee[]>  {
    return this._menteeService.getArchivedMentees().pipe(
      tap(res => {
        this.dataSource.data = res;
        this.dataSource.sort = this.sort;
      }))
  }

  backFromArchive(id: string) {
    this._menteeService.backFromArchive(id).pipe(
      switchMapTo(this.getMentees())
    ).subscribe()
  }

  removePermanently(id: string) {
    this._menteeService.removePermanently(id).pipe(
      switchMapTo(this.getMentees())
    ).subscribe()
  }
}
