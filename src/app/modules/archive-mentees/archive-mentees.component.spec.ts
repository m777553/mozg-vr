import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArchiveMenteesComponent } from './archive-mentees.component';

describe('ArchiveMenteesComponent', () => {
  let component: ArchiveMenteesComponent;
  let fixture: ComponentFixture<ArchiveMenteesComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ArchiveMenteesComponent]
    });
    fixture = TestBed.createComponent(ArchiveMenteesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
