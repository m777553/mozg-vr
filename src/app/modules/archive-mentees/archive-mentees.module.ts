import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ArchiveMenteesComponent} from "./archive-mentees.component";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {MatSortModule} from "@angular/material/sort";
import {MatTableModule} from "@angular/material/table";
import {MenuModule} from "../../components/shared/menu/menu.module";



@NgModule({
  declarations: [ArchiveMenteesComponent],
    imports: [
        CommonModule,
        MatButtonModule,
        MatIconModule,
        MatSortModule,
        MatTableModule,
        MenuModule,
    ],
  exports: [ArchiveMenteesComponent]
})
export class ArchiveMenteesModule { }
