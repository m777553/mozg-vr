import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoursesComponent } from './courses.component';
import {MenuModule} from "../../components/shared/menu/menu.module";
import {MatExpansionModule} from "@angular/material/expansion";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {HeaderModule} from "../../components/shared/header/header.module";



@NgModule({
  declarations: [
    CoursesComponent
  ],
    imports: [
        CommonModule,
        MenuModule,
        MatExpansionModule,
        MatButtonModule,
        MatIconModule,
        HeaderModule
    ],
  exports: [CoursesComponent]
})
export class CoursesModule { }
