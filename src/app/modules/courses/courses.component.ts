import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {Course} from "../../models/course.model";
import {ActivatedRoute} from "@angular/router";
import {CoursesService} from "../../services/courses/courses.service";
import {BehaviorSubject, take} from "rxjs";

@Component({
  selector: 'mozg-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CoursesComponent implements OnInit {
  courses$ = new BehaviorSubject<Course[]>([])
  editMode = false;
  menteeId: string | null = null

  constructor(private _route: ActivatedRoute, private _coursesService: CoursesService) {
  }

  ngOnInit() {
    this._route.paramMap.subscribe(params => {
      this.menteeId = params.get('id');
      this.editMode = !this.menteeId;
    });
    this._coursesService.getCourses().pipe(take(1)).subscribe(data => {
      this.courses$.next(data)
      console.log(this.courses$.value)
    })
  }

  editCourse(course: any, content: any) {

  }

  addCourse(courseId: string) {
    if (this.menteeId) {
      const data = {id: this.menteeId, exercises: courseId}
      this._coursesService.addCourseToMentee(data)
    }
  }
}
