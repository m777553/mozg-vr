import {AfterViewInit, ChangeDetectionStrategy, Component, OnInit, ViewChild} from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {MenteeService} from '../../services/mentee/mentee.service';
import {filter, Observable, switchMap, switchMapTo, take, tap} from 'rxjs';
import {Course, Mentee} from '../../services/mentee/mentee.model';
import {MatDialog} from '@angular/material/dialog'
import {AddMenteeDialogComponent} from "../../dialogs/add-mentee-dialog/add-mentee-dialog.component";
import {UserService} from "../../services/user/user.service";
import {Router} from "@angular/router";


@Component({
  selector: 'mozg-mentees',
  templateUrl: './mentees.component.html',
  styleUrls: ['./mentees.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenteesComponent implements OnInit, AfterViewInit {
  displayedColumns: string[] = ['surname', 'name', 'age', 'currentCourse', 'progress', 'actions'];
  dataSource = new MatTableDataSource<Mentee>([]);

  @ViewChild(MatSort) sort: MatSort | null = null;

  constructor(private _menteeService: MenteeService, private _matDialog: MatDialog, private _userService: UserService, private _router: Router) {
  }

  ngOnInit() {
    this.getMentees().pipe(take(1)).subscribe()
    this.dataSource.sortingDataAccessor = (item: Mentee, property) => {
      switch (property) {
        case 'progress':
          return this.calculateProgress(item.activeCourse);
        default:
          // @ts-ignore
          return item[property];
      }
    };
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  getMentees(): Observable<Mentee[]> {
    return this._menteeService.getMentees().pipe(
      tap(res => {
        this.dataSource.data = res;
        this.dataSource.sort = this.sort;
      }))
  }

  openAddMenteeDialog() {
    this._matDialog.open(AddMenteeDialogComponent, {
      width: '600px',
      minHeight: '750px'
    })
      .afterClosed()
      .pipe(
        filter(res => res),
        switchMap(res => {
          return this._menteeService.addMentee(res)
        }),
        switchMap(() => this.getMentees()),
        take(1)
      ).subscribe()
  }

  calculateProgress(activeCourse: Course | null): number {
    if (!activeCourse || !activeCourse.exercises) {
      return 0
    }
    const finishedExerciseCount = activeCourse.exercises.filter(ex => ex.isFinished).length
    return Math.round(finishedExerciseCount / activeCourse.exercises.length * 100)
  }

  navigateToMenteePage(menteeId: string) {
    this._router.navigate(['/user', menteeId]);
  }
}
