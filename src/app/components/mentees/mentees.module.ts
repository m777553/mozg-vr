import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenteesComponent } from './mentees.component';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import {MatNativeDateModule} from "@angular/material/core";
import {RouterLink} from "@angular/router";



@NgModule({
    declarations: [
        MenteesComponent,
    ],
    exports: [
        MenteesComponent,
    ],
    imports: [
        CommonModule,
        MatButtonModule,
        MatIconModule,
        MatTableModule,
        MatSortModule,
        MatNativeDateModule,
        RouterLink
    ],
})
export class MenteesModule { }
