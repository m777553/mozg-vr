import {AfterViewInit, ChangeDetectionStrategy, Component, OnInit, ViewChild} from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {OrganizationsService} from '../../services/organizations/organizations.service';
import {Organization} from '../../modules/organizations.model';
import {switchMapTo, take, tap} from "rxjs";

@Component({
  selector: 'mozg-organizations-table',
  templateUrl: './organizations-table.component.html',
  styleUrls: ['./organizations-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OrganizationsTableComponent implements OnInit, AfterViewInit {
  displayedColumns: string[] = ['city', 'name', 'comment', 'balance', 'actions'];
  dataSource = new MatTableDataSource<Organization>([]);

  @ViewChild(MatSort) sort: MatSort | null = null;

  constructor(private _organizationsService: OrganizationsService) {
  }

  ngOnInit() {
    this.getOrganizations().subscribe();
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  getOrganizations() {
    return this._organizationsService.getOrganizations().pipe(
      tap(organizations => {
        this.dataSource.data = organizations;
      })
    );
  }

  deleteOrganization(id: string) {
    this._organizationsService.deleteOrganization(id).pipe(switchMapTo(this.getOrganizations()), take(1)).subscribe()
  }

  navigateToOrganizationPage(id: string) {
    console.log('navi to', id)
  }
}
