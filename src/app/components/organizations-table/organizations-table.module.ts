import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrganizationsTableComponent } from './organizations-table.component';
import {MatTableModule} from "@angular/material/table";
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";
import {MatSortModule} from "@angular/material/sort";



@NgModule({
    declarations: [
        OrganizationsTableComponent
    ],
    exports: [
        OrganizationsTableComponent
    ],
  imports: [
    CommonModule,
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatSortModule
  ]
})
export class OrganizationsTableModule { }
