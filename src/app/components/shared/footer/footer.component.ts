import { Component } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'mozg-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent {
  constructor(private router: Router) { }

  get isLoginPageOrRegisterPage(): boolean {
    return this.router.url === '/log-in' || this.router.url === '/sign-up';
  }
}
