import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './footer.component';
import { MatButtonModule } from '@angular/material/button'
import {RouterLink} from "@angular/router";


@NgModule({
  declarations: [
    FooterComponent
  ],
    imports: [
        CommonModule,
        MatButtonModule,
        RouterLink
    ],
  exports: [
    FooterComponent
  ]
})
export class FooterModule { }
