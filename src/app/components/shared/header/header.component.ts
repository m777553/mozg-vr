import {Component} from '@angular/core';
import {Router} from "@angular/router";
import {UserService} from "../../../services/user/user.service";

@Component({
  selector: 'mozg-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  constructor(private router: Router, private _user: UserService) {
  }

  get isLoginPageOrRegisterPage(): boolean {
    return this.router.url === '/log-in' || this.router.url === '/sign-up';
  }

  navigateToHome() {
    const url = this._user.currentUser?.role === 'admin' ? '/admin-home' : '/user-home'
    this.router.navigate([url]);
  }
}
