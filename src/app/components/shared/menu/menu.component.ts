import {ChangeDetectionStrategy, Component} from '@angular/core';
import {UserService} from '../../../services/user/user.service';
import {Router} from '@angular/router';
import {AuthService} from "../../../services/auth/auth.service";

@Component({
  selector: 'mozg-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MenuComponent {
  constructor(public userService: UserService, private _router: Router, private _authService: AuthService) {
  }


  logout() {
    this.userService.currentUser = null;
    this._router.navigate(['/log-in']);
    this._authService.logout()
  }
}
