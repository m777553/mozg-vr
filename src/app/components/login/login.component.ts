import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth/auth.service';
import {filter, take, tap} from 'rxjs';

@Component({
  selector: 'mozg-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  authForm!: FormGroup;

  constructor(private _formBuilder: FormBuilder, private _router: Router, private _authService: AuthService) {
  }

  ngOnInit(): void {
    this.authForm = this._formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

  login(): void {
    if (this.authForm.valid) {
      this.authForm.disable();
      console.log(this.authForm.value);
      this._authService.login(this.authForm.value).pipe(
        take(1),
        filter(res => !!res),
        tap(({role}) => {
          if (role === 'admin') {
            this._router.navigate(['/admin-home'])
          }
          if (role === 'user' || role === 'localadmin') {
            this._router.navigate(['/user-home'])
          }
        }),
      ).subscribe();
    }
  }

  onSignUp() {
    this._router.navigate(['/sign-up'])
  }
}
