import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {SessionsService} from '../../services/session/sessions.service';
import {Subject, take, takeUntil} from 'rxjs';
import {OrganizationsService} from "../../services/organizations/organizations.service";

@Component({
  selector: 'mozg-admin-home',
  templateUrl: './admin-home.component.html',
  styleUrls: ['./admin-home.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminHomeComponent implements OnInit, OnDestroy {
  private _unsubscribeAll$: Subject<void> = new Subject();

  constructor(private _organizations: OrganizationsService) {
  }

  ngOnInit(): void {
    this._organizations.getOrganizations().pipe(take(1)).subscribe()
  }

  ngOnDestroy(): void {
    this._unsubscribeAll$.next();
    this._unsubscribeAll$.complete();
  }
}
