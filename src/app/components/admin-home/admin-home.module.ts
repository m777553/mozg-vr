import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminHomeComponent } from './admin-home.component';
import { LoginModule } from '../login/login.module';
import { MenuModule } from '../shared/menu/menu.module';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { CurrentSessionModule } from '../current-session/current-session.module';
import {OrganizationsTableModule} from "../organizations-table/organizations-table.module";



@NgModule({
  declarations: [
    AdminHomeComponent
  ],
    imports: [
        CommonModule,
        LoginModule,
        MenuModule,
        MatExpansionModule,
        MatIconModule,
        CurrentSessionModule,
        OrganizationsTableModule,
    ],
  exports: [
  ]
})
export class AdminHomeModule { }
