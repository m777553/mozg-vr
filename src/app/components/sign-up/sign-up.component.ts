import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {filter, Subject, take, takeUntil, tap} from 'rxjs';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth/auth.service';
import {MatStepper} from '@angular/material/stepper';
import {SignUpUserDTO} from '../../models/auth.model';

@Component({
  selector: 'mozg-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class SignUpComponent implements OnInit, OnDestroy {
  signUpForm!: FormGroup;
  confirmForm!: FormGroup;
  moreInfoForm!: FormGroup;
  legalEntityForm!: FormGroup;
  private _unsubscribe$ = new Subject<void>();

  constructor(private _formBuilder: FormBuilder, private _router: Router, private _authService: AuthService) {
  }

  ngOnInit(): void {
    this.signUpForm = this._formBuilder.group({
      login: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', [Validators.required, Validators.minLength(6)]],
      firstName: ['', Validators.required],
      surname: ['', Validators.required],
      phone: ['', Validators.required],
      isLegalEntity: [false]
    }, {validators: this.passwordsMatchValidator});

    this.confirmForm = this._formBuilder.group({
      emailCode: ['', Validators.required],
      smsCode: ['', Validators.required],
    });

    this.moreInfoForm = this._formBuilder.group({
      goal: [''],
      tariff: [''],
    });

    this.legalEntityForm = this._formBuilder.group({
      organizationName: ['', Validators.required],
      inn: ['', Validators.required],
      requisites: ['', Validators.required],
    });
  }

  passwordsMatchValidator(form: FormGroup) {
    return form.get('password')?.value === form.get('confirmPassword')?.value ? null : {mismatch: true};
  }


  next(stepper: MatStepper) {
    stepper.next();
  }

  back(stepper: MatStepper) {
    stepper.previous();
  }

  onSubmit(): void {
    if (this.moreInfoForm.valid || this.legalEntityForm.valid) {
      this.confirmForm.disable();
      this.signUpForm.disable();
      this.moreInfoForm.disable();
      this.legalEntityForm.disable();
      const signUpFields: SignUpUserDTO = {
        email: this.signUpForm.get('login')?.value,
        password: this.signUpForm.get('password')?.value,
        name: this.signUpForm.get('firstName')?.value,
        phone: this.signUpForm.get('phone')?.value,
        surname: this.signUpForm.get('surname')?.value,
        organization: this.moreInfoForm.get('organizationName')?.value,
        goal: this.moreInfoForm.get('goal')?.value,
        tariff: this.moreInfoForm.get('tariff')?.value,
        isLegalEntity: this.signUpForm.get('isLegalEntity')?.value,
        organizationName: this.legalEntityForm.get('')?.value,
        inn: this.legalEntityForm.get('inn')?.value,
        requisites: this.legalEntityForm.get('requisites')?.value,
      };
      this._authService.signUp(signUpFields).pipe(
        take(1),
        filter(res => !!res),
        tap(() => {
          this._router.navigate(['/user-home']);
        }),
      ).pipe(takeUntil(this._unsubscribe$)).subscribe();
    }
  }

  ngOnDestroy(): void {
    this._unsubscribe$.next();
    this._unsubscribe$.complete();
  }
}
