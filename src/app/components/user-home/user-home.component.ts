import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject, takeUntil, tap } from 'rxjs';
import { SessionsService } from '../../services/session/sessions.service';

@Component({
  selector: 'mozg-user-home',
  templateUrl: './user-home.component.html',
  styleUrls: ['./user-home.component.scss']
})
export class UserHomeComponent  implements OnInit, OnDestroy {
  private _unsubscribeAll$: Subject<void> = new Subject();
  constructor(private _sessionService: SessionsService) {
  }

  ngOnInit(): void {
    this._sessionService.getSessions().pipe(
      tap(res=>{
        console.log('res',res);
      }),
      takeUntil(this._unsubscribeAll$)).subscribe();
  }
  ngOnDestroy(): void {
    this._unsubscribeAll$.next();
    this._unsubscribeAll$.complete();
  }
}
