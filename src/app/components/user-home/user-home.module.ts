import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserHomeComponent } from './user-home.component';
import { CurrentSessionModule } from '../current-session/current-session.module';
import { MenuModule } from '../shared/menu/menu.module';
import { MenteesModule } from '../mentees/mentees.module';


@NgModule({
  declarations: [
    UserHomeComponent
  ],
    imports: [
        CommonModule,
        CurrentSessionModule,
        MenuModule,
        MenteesModule,
    ],
})
export class UserHomeModule {}
