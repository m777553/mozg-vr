import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CurrentSessionComponent } from './current-session.component';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';



@NgModule({
    declarations: [
        CurrentSessionComponent,
    ],
    exports: [
        CurrentSessionComponent,
    ],
  imports: [
    CommonModule,
    MatIconModule,
    MatButtonModule,
  ],
})
export class CurrentSessionModule { }
