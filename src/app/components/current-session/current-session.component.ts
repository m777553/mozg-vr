import { Component } from '@angular/core';
import { SessionsService } from '../../services/session/sessions.service';
import { SessionDTO } from '../../models/sessions.model';

@Component({
  selector: 'mozg-current-session$',
  templateUrl: './current-session.component.html',
  styleUrls: ['./current-session.component.scss']
})
export class CurrentSessionComponent {
  constructor(public sessions: SessionsService) {
  }
  calculateCompletionPercentage(session: SessionDTO): string {
    const percentage = (session.finishedTasksCount / session.totalTasksCount) * 100;
    return `${percentage.toFixed(0)}%`;
  }
}
