import {User} from './models/auth.model';
import {SessionDTO} from './models/sessions.model';
import {Course} from "./models/course.model";

export const MOCK_USER: User = {
  id: '1',
  name: 'Covalent Elena',
  role: 'user',
};
export const MOCK_ADMIN: User = {
  id: '2',
  name: 'Medical center on 5th',
  role: 'admin',
};


export const MOCK_SESSIONS: SessionDTO[] = [
  {
    id: 1,
    userId: 101,
    userName: 'Лера Семенова',
    courseId: 6,
    courseName: 'аутизм',
    totalTasksCount: 8,
    finishedTasksCount: 3,
  },
  {
    id: 2,
    userId: 102,
    userName: 'Jane Doe',
    courseId: 202,
    courseName: 'Course 2',
    totalTasksCount: 10,
    finishedTasksCount: 5,
  },
];

export const COURSES = [
  {
    title: 'Название курса',
    content: [
      {
      subTitle: 'Вариант 1',
      description: 'В своём стремлении повысить качество жизни, они забывают, что дальнейшее развитие различных форм деятельности обеспечивает широкому кругу (специалистов) участие в формировании благоприятных перспектив. С другой стороны, высокое качество позиционных исследований предопределяет высокую востребованность инновационных методов управления процессами.'
    },
      {
        subTitle: 'Вариант 2',
        description: 'Внезапно, интерактивные прототипы, инициированные исключительно синтетически, заблокированы в рамках своих собственных рациональных ограничений. Кстати, многие известные личности ограничены исключительно образом мышления. Задача организации, в особенности же перспективное планирование позволяет выполнить важные задания по разработке первоочередных требований.'
      },
      {
        subTitle: 'Вариант 3',
        description: 'Высокий уровень вовлечения представителей целевой аудитории является четким доказательством простого факта: внедрение современных методик требует анализа существующих финансовых и административных условий. Являясь всего лишь частью общей картины, стремящиеся вытеснить традиционное производство, нанотехнологии набирают популярность среди определенных слоев населения, а значит, должны быть разоблачены.'
      }
    ]
  },
  {
    title: 'Другой курс',
    content: [
      {
        subTitle: 'Вариант 1',
        description: 'В своём стремлении повысить качество жизни, они забывают, что дальнейшее развитие различных форм деятельности обеспечивает широкому кругу (специалистов) участие в формировании благоприятных перспектив. С другой стороны, высокое качество позиционных исследований предопределяет высокую востребованность инновационных методов управления процессами.'
      },
    ]
  },
  {
    title: 'Входной тест',
    content: [
      {
        description: 'Входное тестированние'
      },
    ]
  }
]


