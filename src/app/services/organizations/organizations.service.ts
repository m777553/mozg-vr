import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject, Observable, take, tap} from "rxjs";
import {Organization} from "../../modules/organizations.model";

@Injectable({
  providedIn: 'root'
})
export class OrganizationsService {
  allOrganizations$ = new BehaviorSubject<Organization[]>([])

  constructor(private _http: HttpClient) {
  }

  getOrganizations(): Observable<Organization[]> {
    return this._http.get<Organization[]>(`org/GetOrganisations`).pipe(tap(organizations => {
      this.allOrganizations$.next(organizations)
    }))
  }

  deleteOrganization(id: string){
    return this._http.delete(`org/Delete/${id}`).pipe(take(1))
  }
}
