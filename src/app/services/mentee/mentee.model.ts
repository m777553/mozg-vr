export interface Order {
  id: string;
  name: string;
  description: string;
  isFinished: boolean;
}

// TODO move to model directory

export interface Exercise {
  id: string;
  name: string;
  description: string;
  isFinished: boolean;
  orders: Order[];
}

export interface Course {
  id: string;
  name: string;
  description: string;
  isFinished: boolean;
  exercises: Exercise[];
  statistics: number;
}

export interface Mentee {
  id: string;
  isActive: boolean;
  name: string;
  surname: string;
  age: number;
  gender: string;
  interests: string;
  recommendations: number;
  activeCourse: Course;
  passedCourses: number;
  organisation: string;
}

export interface MenteeDTO {
  name: string,
  surname: string,
  age: number,
  gender: string,
  interests: string,
  id: string
}


