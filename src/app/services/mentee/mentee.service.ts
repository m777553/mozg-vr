import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Mentee, MenteeDTO} from './mentee.model';

@Injectable({
  providedIn: 'root'
})
export class MenteeService {

  constructor(private _http: HttpClient) {
  }

  getMentees(): Observable<Mentee[]> {
    return this._http.get<Mentee[]>(`pupils/GetPupils`)
  }

  getMenteeById(id: string): Observable<Mentee> {
    return this._http.get<Mentee>(`pupils/GetPupil/${id}`)
  }

  editMenteeById(id: string, body: Mentee): Observable<ArrayBuffer> {
    // TODO зачем тут добавлять айди, если он в параметре пути?
    // TODO ошибка пренадлежности к организации - зачем?
    body.id = id
    console.log(body)
    return this._http.put<ArrayBuffer>(`pupils/Edit/${id}`, body)
  }

  addMentee(mentee: MenteeDTO): Observable<boolean> {
    return this._http.post<boolean>('pupils/Add', mentee)
  }

  getArchivedMentees(): Observable<Mentee[]> {
    return this._http.get<Mentee[]>(`archive/GetPupils`)
  }

  moveToArchive(id: string): Observable<Mentee[]> {
    // TODO add types to data
    const data = {finalArchivation: true}
    return this._http.post<Mentee[]>(`archive/RemoveToArchive/${id}`, data)
  }

  backFromArchive(id: string): Observable<Mentee> {
    return this._http.get<Mentee>(`archive/RestorePupil/${id}`)
  }

  removePermanently(id: string) {
    // TODO add types to data
    const data = {deleteFromSystem: true}
    return this._http.post<Mentee[]>(`archive/RemoveToArchive/${id}`, data)
  }
}
