// auth.service.ts
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {SignUpUserDTO, User, UserAuth, UserDTO} from '../../models/auth.model';
import {catchError, map, Observable, of, tap} from 'rxjs';
import { UserService } from '../user/user.service';
import { MOCK_USER } from '../../mocks';

@Injectable({
  providedIn: 'root',
})
export class AuthService {

  constructor(private _http: HttpClient, private _userService: UserService) {

  }

  isAuthenticated(): boolean {
    const user = localStorage.getItem('user');
    return !!user;
  }

  login(user: UserAuth): Observable<User> {
    return this._http.post<UserDTO>(`users/auth/login`, user).pipe(
      map((response: UserDTO) => {
        localStorage.setItem('token', response.token);
        localStorage.setItem('user', JSON.stringify(response.user));
        this._userService.currentUser = response.user;
        return response.user
      }),
      catchError(() => of(MOCK_USER)),
    );
  }

  signUp(user: SignUpUserDTO): Observable<User> {
    console.log(user)
    return this._http.post<{token: string; user: User; organization: any}>(`users/sign-up`, user).pipe(
      map(res => {
        localStorage.setItem('token', res.token);
        localStorage.setItem('user', JSON.stringify(res.user));
        return res.user
      }),
      tap(user => this._userService.currentUser = user),
      catchError(() => of(MOCK_USER))
    );
  }

  logout(): void {
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    this._userService.currentUser = null;
  }
}
