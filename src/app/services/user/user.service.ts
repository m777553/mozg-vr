import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { User } from '../../models/auth.model';

const MOCK_USER: User = {
  id: '0',
  name: 'Menu',
  role: 'admin',
};

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private _currentUser$ = new BehaviorSubject<User | null>(null);

  get currentUser() {
    return this._currentUser$.value;
  }

  set currentUser(user: User | null) {
    this._currentUser$.next(user);
    console.log('User is set', user);
  }

  constructor() {
    this.loadUserFromLocalStorage();
  }

  private loadUserFromLocalStorage(): void {
    const user = localStorage.getItem('user');
    if (user) {
      this.currentUser = JSON.parse(user);
    }
  }
}
