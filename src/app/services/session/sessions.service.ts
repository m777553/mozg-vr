import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, of, tap } from 'rxjs';
import { SessionDTO } from '../../models/sessions.model';
import { MOCK_SESSIONS } from '../../mocks';

@Injectable({
  providedIn: 'root',
})
export class SessionsService {
session$= new BehaviorSubject<SessionDTO[]>([]) ;
  constructor(private _http: HttpClient) {
  }

  getSessions() {
    // return this._http.get('sessions');
    return of(MOCK_SESSIONS).pipe(
      tap((sessions) => {
        this.session$.next(sessions);
      }
    ))
  }


}
