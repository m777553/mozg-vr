import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject, Observable, take, tap} from "rxjs";
import {Course} from "../../models/course.model";

@Injectable({
  providedIn: 'root'
})

export class CoursesService {
  allCourses$ = new BehaviorSubject<Course[]>([])

  constructor(private _http: HttpClient) {
  }

  getCourses(id?: string):Observable<Course[]> {
    return this._http.get<Course[]>(`Courses/Courses/GetCourses`).pipe(tap((courses: Course[])=>{
      this.allCourses$.next(courses)
    }))
  }

  addCourseToMentee(data: { id: string; exercises: string; }) {
    console.log(data)
    return this._http.post<boolean>(`pupils/AddCourse/${data.id}`, data.exercises).pipe(take(1), tap(data=>{
      console.log('pipe', data)
    })).subscribe()
  }
}
