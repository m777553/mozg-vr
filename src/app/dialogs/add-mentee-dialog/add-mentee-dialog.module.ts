import {NgModule} from '@angular/core';
import {AddMenteeDialogComponent} from "./add-mentee-dialog.component";
import {CommonModule} from '@angular/common';
import {MatDialogModule} from "@angular/material/dialog";
import {MatTabsModule} from "@angular/material/tabs";
import {ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatSelectModule} from "@angular/material/select";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";

@NgModule({
  declarations: [AddMenteeDialogComponent],
  exports: [AddMenteeDialogComponent],
  imports: [CommonModule, MatDialogModule, MatTabsModule, ReactiveFormsModule, MatFormFieldModule, MatInputModule, MatDatepickerModule, MatSelectModule, MatButtonModule, MatIconModule]
})

export class AddMenteeDialogModule {
}
