import {ChangeDetectionStrategy, Component} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {MatDialogRef} from '@angular/material/dialog';
import {MenteeDTO} from "../../services/mentee/mentee.model";

@Component({
  templateUrl: './add-mentee-dialog.component.html',
  styleUrls: ['add-mentee-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,

})
export class AddMenteeDialogComponent {
  menteeForm = this.fb.group({
    name: ['', Validators.required],
    surname: ['', Validators.required],
    dob: [Date, Validators.required],
    gender: ['', Validators.required],
    interests: ['', Validators.required]
  });

  constructor(public dialogRef: MatDialogRef<AddMenteeDialogComponent>, private fb: FormBuilder) {
  }

  close(): void {
    this.dialogRef.close();
  }

  submit(): void {
    if (this.menteeForm.valid) {
      const dob = this.menteeForm.value.dob;
      // @ts-ignore
      const age = this._calculateAge(dob);
      const menteeDTO: MenteeDTO = {
        name: this.menteeForm.value.name || '',
        surname: this.menteeForm.value.surname || '',
        gender: this.menteeForm.value.gender || '',
        interests: this.menteeForm.value.interests || '',
        id:"",
        age
      }
      this.dialogRef.close(menteeDTO);
    }
  }

  private _calculateAge(dateOfBirth?: Date): number {
    const today = new Date();
    // @ts-ignore
    const birthDate = new Date(dateOfBirth);
    let age = today.getFullYear() - birthDate.getFullYear();
    const monthDifference = today.getMonth() - birthDate.getMonth();
    if (monthDifference < 0 || (monthDifference === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
    return age;
  }
}
